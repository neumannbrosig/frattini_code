#####################################################################################################################

IntegralBasisOfMatrixAlgebraOnMatrices := function(mats)
local n, A, B,list, r, V1, d;

mats := Filtered(mats, x -> x <> IdentityMat(DimensionsMat(x)[1]));
n := DimensionsMat(mats[1])[1];
Add(mats, IdentityMat(n));
A := Algebra(Rationals, mats);
d := Dimension(A);

B := VectorSpace(Rationals,mats);




while Dimension(B) < Dimension(A) do
   mats := Set(List(Tuples(mats,2),Product));
   B := VectorSpace(Rationals,mats);
od;

list:=[];

for r in mats do
V1:=VectorSpace( Rationals, Concatenation(list,[r]) );
if Dimension(V1) > Length(list) then
    Add(list,r);
     if Length(list)=Dimension(A) then
       break;
     fi;
   fi;
od;

return list;
end;

#####################################################################################################################


FrattiniSubgroupPcpGroup := function(G)

local F, N, I, p, hom, C, H, f, r, mat, vec, relevant_primes, list, l, U, gens, pcp, mats, A, J, v, coeffs, n, c, d, M, i;

## --------------------------------------------------- ##
#        Helper function to determine splittings        #
## --------------------------------------------------- ##


f := function (A)
    local sys, c, w;
    sys := CRSystem(A.dim, Length(A.mats), A.char);
    for c in A.enumrels do
        w := CollectedRelatorCR( A, c[1], c[2]);
        if IsBound( A.extension ) then
            AddEquationsCR(sys, w[1], w[2], false);
        else
            AddEquationsCR(sys, w[1], w[2], true);
        fi;
    od;
    return rec(
	system := sys,
        basis := KernelCR(A, sys),
        transl := SpecialSolutionCR(A, sys));
end;


## --------------------------------------------------- ##
#               finite and abelian groups 		        #
## --------------------------------------------------- ##

if IsFinite(G) then
   return FrattiniSubgroup(G);
fi;

if IsAbelian(G) then
   return FrattiniSubgroup(TorsionSubgroup(G));
fi;

## --------------------------------------------------- ##
#           Non-abelian Fitting subgroup 		#
## --------------------------------------------------- ##


F := FittingSubgroup(G);

if not IsAbelian(F) then
   Print("\nDividing out derived subgroup of Fitting subgroup\n");
   N := DerivedSubgroup(F); 
   hom := NaturalHomomorphismByNormalSubgroup(G,N);
   I := Image(hom);
   return PreImage(hom, FrattiniSubgroupPcpGroup(I));
fi;


## --------------------------------------------------- ##
#               Non-trivial normal torsion 				#
#               (step 2 of the algorithm) 				#
## --------------------------------------------------- ##

N := TorsionSubgroup(F);

if not IsTrivial(N) then
   # Find a minimal elementary abelian normal subgroup
   U := N;
   p := Minimum(PrimeDivisors(Size(U)));
   gens := GeneratorsOfGroup(U);
   hom := GroupHomomorphismByImages(U,U,gens, List(gens,x -> x^p));
   U := Kernel(hom); 
   gens := GeneratorsOfGroup(U);
   # U is elementary abelian, but not minimal in general
   if Size(U) > p then
       Print("\nDividing out minimal normal torsion.\nSize of normal torsion subgroup: ");
       Print(Size(N));
       Print("\nPrime divisors of size: ");
       Print(PrimeDivisors(Size(N)));
  
       pcp := Pcp(U,"snf");
	   mats := LinearActionOnPcp(GeneratorsOfGroup(G),pcp)*One(GF(p));
	   M := GModuleByMats(mats,GF(p));
	   v := List(MTX.BasesMinimalSubmodules(M)[1][1],IntFFE);
	   U := NormalClosure(G,Group(Product(List([1..Length(gens)], i -> pcp[i]^v[i]))));
       # now U is minimal
   fi;
   hom := NaturalHomomorphismByNormalSubgroup(G,U);
   I := Image(hom);
   I := PreImage(hom, FrattiniSubgroupPcpGroup(I));
   
   r := CRRecordBySubgroup(G,U);
   N := Intersection(ComplementsCR(r));
   if IsGroup(N) then 
      return Intersection(I,N);
   else
      return I;
   fi;
	  
fi;


## --------------------------------------------------- ##
#                  Non-trivial center 		        	#
#                  (step 3 of the algorithm)        	#
## --------------------------------------------------- ##

C := Center(G); 
# C is free abelian, since we divided out normal torsion 
# in the previous step

if not IsTrivial(C) then
   Print("\nDividing out center\n");
   N := Group(C.1); 

   hom := NaturalHomomorphismByNormalSubgroup(G,N);
   I := Image(hom);
   H := PreImage(hom, FrattiniSubgroupPcpGroup(I));

 
   # Determine the splitting of N in G
   r := CRRecordBySubgroup(G,N);
   mat := TransposedMat(f(r).system.base);
   vec := Concatenation(r.extension);


   if SolutionMat(mat,vec) <> fail then 
      # Case: N splits mod almost all primes 
      return Intersection(H,DerivedSubgroup(G));
   else
      # Case: N splits only modulo finitely primes 
      # Determine relevant primes   
      r := SmithNormalFormIntegerMatTransforms(mat);
      relevant_primes := Filtered(Set(List(vec*r.coltrans,AbsoluteValue)), x -> x > 0);
      relevant_primes := Set(Concatenation(List(relevant_primes, PrimeDivisors)));
      if IsEmpty(relevant_primes) then
         return H;
      else 
          I := G;
          for p in relevant_primes do
             list := List(MaximalSubgroupClassesByIndex(G,p),Representative);
             list := Filtered(list, x -> not IsSubgroup(x,N));
             # All elements of this list are normal subgroups by construction
             I := Intersection(list);
          od;
          return Intersection(I,H);
      fi;
   fi;  
fi;


## --------------------------------------------------- ##
#      Non-Q-semisimple action on (free abelian) F   	#
#           (step 4 of the algorithm)	 			   	#
## --------------------------------------------------- ##

pcp := Pcp(F,"snf");
mats := LinearActionOnPcp(GeneratorsOfGroup(G),pcp);
A := Algebra(Rationals,mats);
J := RadicalOfAlgebra(A);
if Dimension(J) > 0 then
   Print("Action not semisimple\n");
   Print("Dimension of Radical: "); Print(Dimension(RadicalOfAlgebra(A)));Print("\n");
   
   gens := GeneratorsOfGroup(F);
   mats := IntegralBasisOfMatrixAlgebraOnMatrices(mats);
   mats := Basis(A,mats);
   
   N := TrivialSubgroup(G);
   for i in [1..Length(Basis(J))] do
      v := Basis(J)[i];
      coeffs := Coefficients(mats,v);
      n := Lcm(List(coeffs,DenominatorRat));
      c := Product(List([1..Length(gens)], x -> gens[x]^(n*v[1][x])));
      N := ClosureGroup(N,NormalClosure(G,Group(c)));
   od;   

   Print("\nHirsch Length of Subgroup: \n");
   Print(HirschLength(N));
   Print("\n");

   hom := NaturalHomomorphismByNormalSubgroup(G,N);
   I := Image(hom);
   return PreImage(hom, FrattiniSubgroupPcpGroup(I));
   
   Print(mats);
fi;
## --------------------------------------------------- ##
#     			 \phi(G) is trivial						#
## --------------------------------------------------- ##

return TrivialSubgroup(G);

end;
